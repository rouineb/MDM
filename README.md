# __MDM__ :hospital:

> This is a `SyriaCharity` like application . 

# __Technos__

After a long meeting with dev team, we decided to use the following stack :
> * SpringBoot (with Tomcat as the application server)
> * SpringCloud (Probably)
> * Java 8
> * Reactjs
> * Docker
> * Hibernate
> * BDD : Postgresql/Cassandra
> * Liquibase/Flyway (BDD versioning)
> * Jenkins
> * Vavr : not sure yet !
> * Spring

# __Dev Team__ :

* `SAIGHI Lydia  : ` 
* `ROUINEB Hamza : rouineb.work@gmail.com`

# __[Git](https://git-scm.com/documentation) rules__ : 

> All git branches are protected, please refer to the project setting, but in general : 
> * Master branch  : Only masters are allowrd to push and merge
> * Develop branch : Everyone is allowed (best practice is to create your own branch with the feature name on it) 
> * Release branch : Only masters are allowed to push and merge 

> Please configure your identity before commiting : 
> ``` console 
$ git config --[local/global] user.name "FIRST-NAME LAST-NAME"
```
> ``` console 
$ git config --[local/global] user.email "EMAIL @"
```
> All git commits should disable the `Fast Forward` [FF](https://git-scm.com/docs/git-merge) merge, to have clean logs :
> either append each time the `--no-ff` flag to the commit CMD
>``` console
 $ git commit --no-ff -m "XXXX.."
```
> or disable it globaly : 
> ``` console 
 $ git config --global merge.ff false
```
> Commits should follow [the AngularJS Git Commit Message Conventions](https://gist.github.com/stephenparish/9941e89d80e2bc58a153).

# __[Docker](https://docs.docker.com/engine/reference/builder/) rules__ : 

> Please use docker private images of `Mchina` project : 
> To pull a given private docker image from `Mchina` project use the following syntax : 
> ``` console
 $ docker pull registry.gitlab.com/devlifealways/mdm[/image_name[:tag]] 
```

# __Accessing Prod/Val Server__ : 
> To access the server via ssh you should do the following : `ssh [username]@machine_ip_address`,  where 
> ```console
 $ ssh `ssh_login`@not_yet
```
> To know the default password please contact the administrator
> It is best to change the default password once conected for the first time 
>```console
 $ passwd $(whoami)
```
> Please note that X11 forwarding was enabled for the sake of using gui apps : 
> ``` console
 $ ssh -X `login`@machine 
```
s
# __General notes__ :  

* Please note, that this project uses a very specific git flow, so that each member could work separetly on his feature without problem.
[GIT FLOW](http://nvie.com/posts/a-successful-git-branching-model/).

* The technologies used here, are skeptical to be changed any time, it all depends on the tasks and the need expressed during the dev phase.

* It is a best practice to put in place a few [snippets](https://gitlab.com/devlifealways/Mchina/snippets) of code to make sure everyone understands exactly your point of view, and gives us a clear understanding of how to do things best.

* Only Comments in __English__ will be accepted ! no other __language__ is permited for the time bieng !
