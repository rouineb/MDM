const path = require('path');
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpackBootstrap = require('./webpack.bootstrap.config')

// is it prod env
let isProd = process.env.NODE_ENV === "production"


// possible multi loaders : see https://www.npmjs.com/package/extract-text-webpack-plugin-fix2450
let extractSCSS = new ExtractTextPlugin({
    filename: './style/[name].css',
    disable: isProd,
    allChunks: true,
    ignoreOrder: false
});

let bootstrapConfig = isProd ? webpackBootstrap.prod : webpackBootstrap.dev

module.exports = {
    entry: {
        app: './app.js',
        bootstrap: bootstrapConfig
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    //devtool: 'inline-source-map',
    watch: true,
    performance: {
        hints: "warning"
    },
    resolve: {
        descriptionFiles: ["package.json"],
        modules: ["node_modules", path.resolve(__dirname, "src")]
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: [/node_modules/],
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.scss$/i,
                loader: extractSCSS.extract(['css-loader', 'sass-loader'])
            },
            {
                test: /\.bundle\.js$/,
                loader: 'bundle-loader',
                options: {
                    lazy: true
                }
            },
            // bootstrap icons
            {
                test: /\.(woff2?|svg)$/,
                loader: 'url-loader?limit=10000&name=fonts/[name].[ext]'
            },
            {
                test: /\.(ttf|eot)$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            },
            // Bootstrap 4
            {
                test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/,
                loader: 'imports-loader?jQuery=jquery'
            },
        ],
    },
    // enable the minify process
    plugins: [
        // should be enabled only in dev mode !
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new UglifyJsPlugin({ cache: true, parallel: true, sourceMap: false }),
        new HtmlWebpackPlugin({
            title: 'ReactJS Dojo',
            filename: 'index.html',
            template: './src/templates/index.html',
            inject: 'body',
            minify: {
                collapseWhitespace: true,
                //removeEmptyAttributes: true
            }
        }),
        extractSCSS,
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            // In case you imported plugins individually, you must also require them here:
            Util: "exports-loader?Util!bootstrap/js/dist/util",
            Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
            Tooltip: "exports-loader?Dropdown!bootstrap/js/dist/tooltip",
        })
    ]

};
